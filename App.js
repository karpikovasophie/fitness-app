import React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet } from 'react-native';

import Auth from "./src/routing/Auth";
import Home from "./src/routing/Home";

import deviceStorage from './src/services/deviceStorage';
import {NavigationContainer} from "@react-navigation/native";
import {navigationRef} from "./src/routing/RootNavigation";
import {createStackNavigator} from "@react-navigation/stack";
import trainsReducer from './src/state/reducers/trains'


import { Provider } from 'react-redux';
import { createStore } from 'redux';

const Stack = createStackNavigator();

const store = createStore(trainsReducer);

export default class App extends React.Component {

    constructor() {
        super();
        this.state = {
            token: deviceStorage.loadJwt(),
            loading: true
        };

        this.newJWT = this.newJWT.bind(this);
        this.deleteJWT = this.deleteJWT.bind(this);

        // this.loadJWT = deviceStorage.loadJwt.bind(this);
    }

    newJWT(token){
        this.setState({
            token: token
        });
    }

    deleteJWT() {
        deviceStorage.deleteJWT();
        this.setState({
            token: ''
        });
    }

    render() {
        if (!this.state.token) {
            return (
                <Provider store={store}>
                    <Auth newJWT={this.newJWT}/>
                </Provider>
            );
        } else {
            return (
                <Provider store={store}>
                    <Home deleteJWT={this.deleteJWT}/>
                </Provider>
            )
        }
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
