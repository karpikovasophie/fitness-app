import React, { useState } from 'react';
import { StyleSheet, View, Image, Alert } from 'react-native';
import {Button, Card, FAB, Input, Text} from 'react-native-elements';

import {LinearGradient} from "expo-linear-gradient";

import axios from 'axios';
import deviceStorage from '../services/deviceStorage';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null
        };
    }

    setEmail(email) {
        this.setState({
            email: email
        })
    }

    setPassword(password) {
        this.setState({
            password: password
        })
    }

    login() {
        const { email, password } = this.state;

        this.setState({ error: '', loading: true });

        axios.post("http://192.168.0.13:5000/api/auth/login",{
            email: email,
            password: password
        })
            .then((response) => {
                deviceStorage.saveKey("token", response.data.token);
                this.props.route.params.newJWT(response.data.token);
                console.log('TOKEN', response.data.token)
            })
            .catch((error) => {
                if (error.response.status === 400) {
                    Alert.alert(
                        "Ошибка входа",
                        error.response.data.message,
                        [
                            {
                                text: "Ок",
                                style: "cancel"
                            },
                        ]
                    );
                }
            });
    }


    render() {

        return (
            <View style={styles.container}>

                <LinearGradient
                    colors={['#6633ff', 'transparent']}
                    style={styles.background}
                />

                <Image source ={require('../images/test.png')}
                       style={{width: "50%" ,height: "30%"}}
                />

                <View style={ styles.formContainer }>

                    <Text
                        h3
                        style={{
                            fontSize:30,
                            textAlign:'center',
                            color: 'white'
                        }}
                    >
                        Добро пожаловать в BESporty!
                    </Text>

                    <Text
                        style={{
                            textAlign:'center',
                            opacity: 0.9,
                            color: 'white',
                            fontSize: 14,
                            padding: 15
                        }}
                    >
                        Мечтаете сделать мышцы рельефными или просто хотите быть в хорошей форме? Присоединяйтесь к BESporty, чтобы планировать занятия в спортзале и дома.
                    </Text>

                    <Card containerStyle={{ borderRadius: 10 }}>

                        <Input
                            placeholder='Email'
                            leftIcon={{ type: 'font-awesome', name: 'envelope', color: 'purple', paddingRight: 10, size: 15 }}
                            value={this.state.email}
                            onChangeText={(text) => this.setEmail(text)}
                        />

                        <Input
                            placeholder='Пароль'
                            leftIcon={{ type: 'font-awesome', name: 'lock', color: 'purple', paddingRight: 10 }}
                            secureTextEntry={true}
                            value={this.state.password}
                            onChangeText={(text) => this.setPassword(text)}
                        />

                        <FAB
                            title="Войти"
                            buttonStyle={{
                                width: 300
                            }}
                            style={{ marginTop: 10 }}
                            onPress={() =>
                                this.login()
                            }
                        />

                        <Button
                            title="Новый пользователь"
                            type="clear"
                            containerStyle={{ marginTop: 10 }}
                            onPress={() => {
                                this.props.navigation.navigate('SignUp')
                            }}
                        />
                    </Card>

                </View>

            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#EE82EE',
        paddingTop:  60

    },

    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 700,
    },

    formContainer: {
        flex: 1,
        width: '100%',
        padding: 10,
        paddingTop: 0
    }
});

export default Login;
