import React, { useState } from 'react';
import { StyleSheet, View, Image,  Alert } from 'react-native';
import { Button, FAB, Input, Text } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';

import { Card } from 'react-native-elements';
import axios from 'axios';
import deviceStorage from '../services/deviceStorage';


class SignUp extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            passwordConfirmation: null,

        };
    }

    setEmail(email) {
        this.setState({
            email: email
        })
    }

    setPassword(password) {
        this.setState({
            password: password
        })
    }

    setPasswordConfirmation(password) {
        this.setState({
            passwordConfirmation: password
        })
    }

    signUp() {
        const { email, password, passwordConfirmation } = this.state;

        this.setState({ error: '', loading: true });

        if (password !== passwordConfirmation) {
            Alert.alert(
                "Ошибка регистрации",
                'Пароли не совпадают',
                [
                    {
                        text: "Ок",
                        style: "cancel"
                    },
                ]
            );
            return;
        }

        axios.post("http://192.168.0.13:5000/api/auth/sign-up",{
            email: email,
            password: password
        })
            .then((response) => {
                Alert.alert(
                    "Пользователь создан",
                    `Пользователь ${email} успешно создан!`,
                    [
                        {
                            text: "Ок",
                            style: "cancel"
                        },
                    ]
                );

                this.props.navigation.navigate('Login');
            })
            .catch((error) => {
                if (error.response.status === 400) {
                    this.setState( { error: 'Следующие ошибки при регистрации: \n' });
                    error.response.data.errors.forEach(e => {
                        let { error } = this.state;
                        error += e.msg + '\n';
                        this.setState( { error: error });
                    });
                    Alert.alert(
                        "Ошибка регистрации",
                        this.state.error,
                        [
                            {
                                text: "Ок",
                                style: "cancel"
                            },
                        ]
                    );
                }
            });
    }

    render() {
        return (
            <View style={styles.container}>

                <LinearGradient
                    colors={['#6633ff', 'transparent']}
                    style={styles.background}
                />

                <Image source ={require('../images/test.png')}
                       style={{width: "50%" ,height: "30%"}}
                />

                <View style={ styles.formContainer }>

                    <Text
                        h3
                        style={{
                            fontSize:30,
                            textAlign:'center',
                            color: 'white',
                            padding: 20,
                            paddingTop: 0
                        }}
                    >
                        Регистрация{"\n"}в BESporty!
                    </Text>



                    <Card containerStyle={{ borderRadius: 10 }}>

                        <View>
                            <Input
                                placeholder='Email'
                                leftIcon={{ type: 'font-awesome', name: 'envelope', color: 'purple', paddingRight: 10, size: 15 }}
                                value={this.state.email}
                                onChangeText={(text) => this.setEmail(text)}
                            />

                            <Input
                                placeholder='Пароль'
                                leftIcon={{ type: 'font-awesome', name: 'lock', color: 'purple', paddingRight: 10 }}
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={(text) => this.setPassword(text)}
                            />

                            <Input
                                placeholder='Повторите пароль'
                                leftIcon={{ type: 'font-awesome', name: 'lock', color: 'purple', paddingRight: 10 }}
                                secureTextEntry={true}
                                value={this.state.passwordConfirmation}
                                onChangeText={(text) => this.setPasswordConfirmation(text)}
                            />

                            <FAB
                                title="Зарегистрироваться"
                                buttonStyle={{
                                    width: 300
                                }}
                                style={{ marginTop: 10 }}
                                onPress={() =>
                                    this.signUp()
                                }
                            />

                            <Button
                                title="Вход"
                                type="clear"
                                containerStyle={{ marginTop: 10 }}
                                onPress={() => {
                                    this.props.navigation.navigate('Login')
                                }}
                            />
                        </View>

                    </Card>

                </View>

            </View>
        );
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#EE82EE',
        paddingTop:  60

    },

    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 700,
    },

    formContainer: {
        flex: 1,
        width: '100%',
        padding: 10,
        paddingTop: 0
    }
});

// ...

export default SignUp;
