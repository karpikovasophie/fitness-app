import React from 'react';
import { StyleSheet, View, Image, TextInput } from 'react-native';
import { Button, FAB, Input, Text, Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import { CheckBox } from 'react-native-elements'
import { Header } from 'react-native-elements';

class TrainStep3 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#6633ff', 'transparent']}
                    style={styles.background}
                />

                <Header
                    leftComponent={ <Icon
                        name='arrow-back'
                        type='ionicon'
                        color="white"
                        size={30}
                        onPress={() =>
                            this.props.navigation.navigate('TrainStep1')
                        }
                    />
                    }
                    centerComponent={ <Icon
                        raised
                        name='barbell'
                        type='ionicon'
                        color="#6633ff"
                        size={20}

                    />}
                    backgroundColor="transparent"
                    containerStyle={{ borderBottomWidth: 0 }}
                />

                <View style={styles.trainContainer}>

                    <Text h4 h4Style={styles.title}>
                        Шаг 3
                    </Text>



                    <Text h3 h3Style={styles.title}>
                        Над чем хотите поработать?
                    </Text>

                    <CheckBox
                        title='Руки'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        containerStyle={styles.mode}
                    />

                    <CheckBox
                        title='Пресс'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        containerStyle={styles.mode}
                    />

                    <CheckBox
                        title='Ягодицы'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        containerStyle={styles.mode}
                    />

                    <CheckBox
                        title='Ноги'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        containerStyle={styles.mode}
                    />

                    <FAB
                        title="Продолжить"
                        buttonStyle={{
                            width: 200,
                            backgroundColor: "#6633ff",
                        }}
                        style={styles.submitButton}
                        onPress={() =>
                            this.props.navigation.navigate('TrainStep1')
                        }
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#EE82EE',

    },

    header: {

    },
    controls: {
        width: "100%",
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 700,
    },
    mode: {
        borderRadius: 15,
        width: "100%"
    },
    title: {
        color: "white",
        textAlign: 'center',
        marginBottom: 50
    },
    trainContainer: {
        flex: 1,
        width: '100%',
        padding: 30,
        paddingTop: 50
    },
    submitButton: {
        marginTop: 20,
        position: 'absolute',
        bottom: 50,
        alignSelf: 'center'
    }
});

// ...

export default TrainStep3;
