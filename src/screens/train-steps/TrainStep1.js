import React from 'react';
import { StyleSheet, View } from 'react-native';
import { FAB, Text, Icon } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import { CheckBox } from 'react-native-elements'
import { Header } from 'react-native-elements';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeMode } from "../../state/actions/trains";

import * as RootNavigation from "../../routing/RootNavigation";

class TrainStep1 extends React.Component {
    constructor() {
        super();
        this.state = {
            modes: [
                {
                    type: 'auto',
                    title: 'Автоматически',
                    checked: true
                },
                {
                    type: 'self',
                    title: 'Самостоятельно',
                    checked: false
                }
            ],
        };
    }

    componentDidMount() {
        this.toggleModes(this.props.mode);
    }

    clearModes() {
        const checkboxes = this.state.modes;
        checkboxes.forEach(c => c.checked = false);
        this.setState({ checkboxes });
    }

    toggleModes(type) {
        this.clearModes();

        const changedCheckbox = this.state.modes.find(m => m.type === type);
        changedCheckbox.checked = true;

        const checkboxes = Object.assign({}, this.state.modes, changedCheckbox);
        this.setState({ checkboxes });
        this.props.changeMode(type);
    }

    goToNextStep() {
        const checkedMode = this.state.modes.find(m => m.checked);

        if (checkedMode.title === 'auto') {
            // TODO: auto step
        } else {
            RootNavigation.navigate('TrainStepSelf2')
        }
    }

    renderModes() {
        return this.state.modes.map(m => {
            return (
                <CheckBox
                    key={m.title}
                    title={m.title}
                    checked={m.checked}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    containerStyle={styles.mode}
                    onPress={() => this.toggleModes(m.type)}
                />
            );
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#6633ff', 'transparent']}
                    style={styles.background}
                />

                <Header
                    leftComponent={ <Icon
                        name='arrow-back'
                        type='ionicon'
                        color="white"
                        size={30}
                        onPress={() => { RootNavigation.goBack() }}
                    />
                    }
                    centerComponent={ <Icon
                        raised
                        name='barbell'
                        type='ionicon'
                        color="#6633ff"
                        size={20}

                    />}
                    backgroundColor="transparent"
                    containerStyle={{ borderBottomWidth: 0 }}
                />

                <View style={styles.trainContainer}>

                    <Text h4 h4Style={styles.title}>
                        Шаг 1
                    </Text>

                    <Text h3 h3Style={styles.title}>
                        Выберите режим тренировки
                    </Text>

                    {this.renderModes()}

                    <FAB
                        title="Продолжить"
                        buttonStyle={{
                            width: 200,
                            backgroundColor: "#6633ff",
                        }}
                        style={styles.submitButton}
                        onPress={() => this.goToNextStep()}
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#EE82EE',

    },

    controls: {
        width: "100%",
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 700,
    },
    mode: {
        borderRadius: 15,
        width: "100%"
    },
    title: {
        color: "white",
        textAlign: 'center',
        marginBottom: 30
    },
    trainContainer: {
        flex: 1,
        width: '100%',
        padding: 30,
    },
    submitButton: {
        marginTop: 20,
        position: 'absolute',
        bottom: 50,
        alignSelf: 'center'
    }
});

// ...

const mapStateToProps = (state) => {
    return { mode: state.trains.mode };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        changeMode,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(TrainStep1);
