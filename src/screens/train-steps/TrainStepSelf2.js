import React from 'react';
import { ScrollView, StyleSheet, View, CheckBox } from 'react-native';
import { FAB, Text, Icon, Card } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import { Button } from 'react-native-elements'
import { Header } from 'react-native-elements';
import { ListItem, Avatar, Overlay } from 'react-native-elements'
import { encode as btoa} from 'base-64';

import libAjax from '../../services/libAjax';
import * as RootNavigation from "../../routing/RootNavigation";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {changeMode} from "../../state/actions/trains";


class TrainStepSelf2 extends React.Component {

     constructor() {
        super();
        this.state = {
            exercises: [],
            modalAddExercisesShow: false,
            token: '',
        };

        this.getExercises();
    }

    componentDidMount() {

             libAjax.get("trains/exercises")
                 .then((response) => {
                     let exercises = response.data;

                     exercises.forEach(e => {
                         e.images.forEach(i => {
                             i.base64Image = `data:image/jpeg;base64,${this.arrayBufferToBase64(i.data.data)}`;
                         });
                         e.checked = false;
                     });

                     this.setState({ exercises });
                 })
                 .catch((error) => {

                     if (error.response.status === 401) {
                         this.props.route.params.deleteJWT()
                     }
                 });

    }

    arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return btoa(binary);
    };


    async getExercises() {

        libAjax.get("trains/exercises")
            .then((response) => {
                let exercises = response.data;

                exercises.forEach(e => {
                    e.images.forEach(i => {
                        i.base64Image = `data:image/jpeg;base64,${this.arrayBufferToBase64(i.data.data)}`;
                    });
                    e.checked = false;
                });

                this.setState({ exercises });
            })
            .catch((error) => {

                if (error.response.status === 401) {
                    this.props.route.params.deleteJWT()
                }
            });
    }

    startAddExercises() {
        this.setState({
            modalAddExercisesShow: true
        })
    }

    saveExercises() {
        this.setState({
            modalAddExercisesShow: false
        })
    }

    toggle(id) {
        let exercises = this.state.exercises.map(e => {
            if (e.id === id) {
                e.checked = !e.checked;
            }
            return e;
        });

        this.setState({ exercises });
    }

    renderAllExercises() {
        return this.state.exercises.map(e => {
            return (
                <ListItem
                    key={e.id}
                    linearGradientProps={{
                        colors: e.checked ? ['#9900FF', '#6666FF'] : ['#DCDCDC', '#DCDCDC'],
                        start: { x: 1, y: 0 },
                        end: { x: 0.1, y: 0 },
                    }}
                    ViewComponent={LinearGradient} // Only if no expo
                    containerStyle={{ borderRadius: 10, marginBottom: 5, padding: 10 }}
                >
                    <Avatar
                        size="medium"
                        source={{uri:  e.images[0].base64Image}}
                    />

                    <ListItem.Content>
                        <ListItem.Title>
                            <Text style={{ color: e.checked ? 'white' : 'black', fontWeight: 'bold' }}>{e.name}</Text>
                        </ListItem.Title>
                    </ListItem.Content>
                    <CheckBox
                        value={e.checked} style={{ color: 'white' }}
                        tintColors={{ true: 'white', false: 'black' }}
                        onValueChange={() => { this.toggle(e.id) }}
                    />
                </ListItem>
            )
        })
    }

    renderSelectedExercises() {
        return this.state.exercises.map(e => {
            if (e.checked)
                return (
                    <ListItem
                        key={e.id}
                        linearGradientProps={{
                            colors: ['#9900FF', '#6666FF'],
                            start: { x: 1, y: 0 },
                            end: { x: 0.1, y: 0 },
                        }}
                        ViewComponent={LinearGradient} // Only if no expo
                        containerStyle={{ borderRadius: 10, marginBottom: 5, padding: 10 }}
                    >
                        <Avatar
                            size="medium"
                            source={{uri:  e.images[0].base64Image}}
                        />

                        <ListItem.Content>
                            <ListItem.Title>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{e.name}</Text>
                            </ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron color="white" />
                    </ListItem>
                )
            })
    }
    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#6633ff', 'transparent']}
                    style={styles.background}
                />

                <Header
                    leftComponent={ <Icon
                        name='arrow-back'
                        type='ionicon'
                        color="white"
                        size={30}
                        onPress={() =>  RootNavigation.goBack() }
                    />
                    }
                    centerComponent={ <Icon
                        raised
                        name='barbell'
                        type='ionicon'
                        color="#6633ff"
                        size={20}

                    />}
                    backgroundColor="transparent"
                    containerStyle={{ borderBottomWidth: 0 }}
                />

                <View style={styles.trainContainer}>

                    <Text h4 h4Style={styles.title}>
                        Шаг 2
                    </Text>

                    <Text h3 h3Style={styles.title}>
                        Выберите упражнения и установите нагрузку
                    </Text>

                    <Card containerStyle={{ borderRadius: 10 }}>
                        <ScrollView style={styles.trainsContainer}>

                            { this.renderSelectedExercises() }
                        </ScrollView>

                        <Button
                            title="Выбрать упражнения"
                            type="outline"
                            containerStyle={{ marginTop: 5 }}
                            onPress={() => { this.startAddExercises() }}
                        />
                    </Card>


                    <Overlay
                        isVisible={this.state.modalAddExercisesShow}
                        overlayStyle={styles.overlay}

                    >
                        <View>
                            <View style={{ alignItems: 'center', marginBottom: 10 }}>
                                <Text style={{fontWeight: "bold", fontSize: 16}}>Упражнения</Text>
                            </View>
                                <ScrollView style={{ maxHeight: 510 }}>
                                    { this.renderAllExercises() }
                                </ScrollView>

                            <Button
                                title="Сохранить"
                                type="outline"
                                containerStyle={{ marginTop:5 }}
                                onPress={() => { this.saveExercises() }}
                            />
                        </View>

                    </Overlay>

                    <FAB
                        title="Продолжить"
                        buttonStyle={{
                            width: 200,
                            backgroundColor: "#6633ff",
                        }}
                        style={styles.submitButton}
                        onPress={() =>
                            this.props.navigation.navigate('TrainStep3')
                        }
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#EE82EE',
    },

    controls: {
        width: "100%",
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 700,
    },
    mode: {
        borderRadius: 15,
        width: "100%"
    },
    title: {
        color: "white",
        textAlign: 'center',
        marginBottom: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    trainContainer: {
        flex: 1,
        width: '100%',
        padding: 10,
    },

    trainsContainer: {
        backgroundColor: 'white',
        maxHeight: 350,
    },

    submitButton: {
        marginTop: 20,
        position: 'absolute',
        bottom: 50,
        alignSelf: 'center'
    },

    overlay: {
        width: '85%',
        height: '75%'
    },

    modalView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
});

// ...
const mapStateToProps = (state) => {
    return { exercises: state.trains.exercises };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        changeMode,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(TrainStepSelf2);
