import * as React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import { FAB } from 'react-native-elements';
import { Icon } from 'react-native-elements'

import * as RootNavigation from '../routing/RootNavigation';

import { connect } from 'react-redux';
import {bindActionCreators} from "redux";
import {clear} from "../state/actions/trains";

class Trains extends React.Component{

    startNewTrain() {
        this.props.clear();
        RootNavigation.navigate('TrainStep1');
    }
    render() {
        return (
            <View style={styles.container}>
                <Text>Тренировки</Text>
                <FAB
                    icon={
                        <Icon
                            name="add"
                            size={25}
                            color="white"
                        />
                    }
                    style={styles.addButton}
                    onPress={() => this.startNewTrain()}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    addButton: {
        position: 'absolute',
        right: 20,
        bottom: 20
    },

});
const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        clear,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Trains);
