import axios from "axios";
import deviceStorage from './deviceStorage';


const libAjax = axios.create({
    baseURL: 'http://192.168.0.13:5000/api/',
    withCredentials: true,
    headers: {
        'Content-Type': 'application/json',
    }
});


libAjax.interceptors.request.use(
    async config => {
        config.headers['Authorization'] = `Bearer ${await deviceStorage.loadJwt() || '0'}`;
        return config;
    },
    error => {
        Promise.reject(error)
    }
);

export default libAjax;
