import { AsyncStorage } from 'react-native';

const deviceStorage = {
    async saveKey(key, value) {
        try {
            await AsyncStorage.setItem(key, value);
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    },

    async loadJwt() {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                return value;
            }

        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    },

    async deleteJWT() {
        try{
            await AsyncStorage.removeItem('token')
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    }
};


export default deviceStorage;
