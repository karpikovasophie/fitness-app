import Login from "../screens/Login";
import SignUp from "../screens/SignUp";
import {NavigationContainer} from "@react-navigation/native";
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {navigationRef} from "./RootNavigation";

const Stack = createStackNavigator();
class Auth extends React.Component{
    render() {
        return (
            <NavigationContainer ref={navigationRef}>
                <Stack.Navigator>
                    <Stack.Screen
                        name="Login"
                        component={Login}
                        options={{headerMode: 'none', headerShown : false}}
                        initialParams={{ newJWT: this.props.newJWT }}
                    />
                    <Stack.Screen
                        name="SignUp"
                        component={SignUp}
                        options={{headerMode: 'none', headerShown : false}}
                    />
                </Stack.Navigator>
             </NavigationContainer>
        )
    }

}

export default Auth;
