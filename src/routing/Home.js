import React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Icon} from "react-native-elements";
import {AsyncStorage, Text, View} from "react-native";

import {navigationRef} from "./RootNavigation";

import TrainStep1 from "../screens/train-steps/TrainStep1";
import Trains from "../screens/Trains";
import TrainStep2 from "../screens/train-steps/TrainStep2";
import TrainStepSelf2 from "../screens/train-steps/TrainStepSelf2";
import deviceStorage from "../services/deviceStorage";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

// Экраны формирования тренировки
const generateTrainScreens = {
    TrainStep1: TrainStep1,

    // Самостоятельный режим
    TrainStepSelf2: TrainStepSelf2,
};

// Экраны проведения тренировки
const trainScreens = {};

function Profile({ navigation }) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>User Profile!</Text>
        </View>
    );
}

class HomeTabs extends React.Component {
    render() {
        return (
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;
                        if (route.name === 'Trains') {
                            iconName = 'barbell';
                        } else if (route.name === 'Profile') {
                            iconName = 'person';
                        }
                        return <Icon name={iconName} size={size} color={color} type='ionicon'/>;
                    },
                })}

                tabBarOptions={{
                    activeTintColor: '#6633ff',
                    inactiveTintColor: 'gray',
                }}
            >
                <Tab.Screen
                    name="Trains"
                    component={Trains}
                    options={({ navigation, route }) => ({
                        title: 'Тренировки',
                    })}

                />

                <Tab.Screen
                    name="Profile"
                    component={Profile}
                    options={({ navigation, route }) => ({
                        title: 'Пользователь',
                    })}

                />

            </Tab.Navigator>
        );
    }
}

class Home extends React.Component  {
    constructor() {
        super();
    }

    render() {
        return (
            <NavigationContainer ref={navigationRef}>
                <Stack.Navigator
                    initialRouteName="HomeTabs"
                    headerMode="none"
                >
                    <Stack.Screen
                        name="HomeTabs"
                        component={HomeTabs}

                    />

                    {Object.entries({
                        ...generateTrainScreens,
                        ...trainScreens,
                    }).map(([name, component]) => (
                        <Stack.Screen
                            name={name}
                            component={component}
                            key={name}
                            initialParams={{ deleteJWT: this.props.deleteJWT }}
                        />
                    ))}

                </Stack.Navigator>
            </NavigationContainer>
        );
    }

}

export default Home;
