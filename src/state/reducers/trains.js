import { combineReducers } from 'redux';
import { CHANGE_MODE, CLEAR } from '../constants';

const INITIAL_STATE = {
    mode: 'auto',
    level: null,
    aim: null,
    type: [],
    exercises: []
};

const changeMode = (state, action) => {
    const newState = { mode: action.payload };
    return newState;
};

const trainsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_MODE:
            const newState = { mode: action.payload };
            return newState;
        case CLEAR:
            return INITIAL_STATE;
        default:
            return state
    }
};

export default combineReducers({
    trains: trainsReducer
});
