import {
    CHANGE_MODE,
    CLEAR,
    SET_EXERCISES
} from '../constants';

export const clear = () => ({
    type: CLEAR
});

export const changeMode = mode => ({
    type: CHANGE_MODE,
    payload: mode
});

export const setExercises = exercises => ({
    type: SET_EXERCISES,
    payload: exercises
});
